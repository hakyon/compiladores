/*******************************************************************/
/*Autor: Alan Azevedo Bancks                                       */
/*                                                                 */
/*Projeto:Analisador Lexico para uma calculadora                   */
/*Data:10/03/2018                                                  */
/*******************************************************************/

#include <stdio.h>
#include <string.h>

char* expressao = "(12*13)+1" ;
//char* expressao = "((12*1.3)+1-(2+5))/5" ;


void anali_lex(char);
char prox_char();
int est = 1;

int main (){
    printf("-Iniciando analise da expressao : %s \n",expressao);
    
    char ch;
    size_t tam_expressao = strlen(expressao);
    for(int i=0;i<=tam_expressao;i++){
        ch = expressao[i];
        //printf("%c \n",ch);
        anali_lex(ch);
    }
    ch = '\n';
    anali_lex(ch);

    printf("Processou toda expressao \n");
    return 0;
}

void erro(char simbolo){
    printf("-Simbolo descartado %c \n",simbolo);
}
void anali_lex(char ch) {

    printf("Estado:%d \n",est);
    switch(est){
    case 1:
        if (ch >= '0' && ch <= '9'){
            est = 2;
            return;
        }
        else if (ch == '+'){
            est = 7;
            break;
        }
        else if (ch == '-'){
            est = 8;
            return;
        }
        else if (ch == '/'){
            printf("DIV \n");
            est = 1;
            return;
        }
        else if (ch == '*'){
            printf("MULTI \n");
            est = 1;
            return;
        }
        else if (ch == '\s' || ch == '\t' || ch == '\n'){
            printf("FIM \n");
            est = 1;
            return;
        }
        else if (ch == '('){
            printf("AB_PAR \n");
            est = 1;
            return;
        }
        else if (ch == ')'){
            printf("FE_PAR \n");
            est = 1;
            return;
        }
        else if (ch == '='){
            est = 6;
        }
        else{
            erro(ch);
            return;
        }

    case 2:
        if(ch >= '0' && ch <= '9'){
            est = 2;
            return;
        }
        else if(ch == '.' || ch == ','){
            est = 3;
            return;
        }
        else{
            printf("INT \n");
            est = 1;
            anali_lex(ch);
            break;
        }
    case 3:
        if(ch >= '0' && ch <= '9'){
            est = 4;
            return;
        }
        else{
            erro(ch);
            return;
        }
    case 4:
        if(ch >= '0' && ch <= '9'){
            est = 4;
            return;
        }
        else{
            printf("FLOAT \n");
            est = 1;
            anali_lex(ch);
            break;
            //return;
        }
    case 5:
        if(ch == '='){
            printf("OP_IGUALDADE \n");
            est = 1;
            return;
        }
        else {
            printf("OP_MAIOR_IGUAL \n");
            est = 1;
            return;
        }
    case 6:
        if(ch == '='){
            printf("OP_MENOR_IGUAL \n");
            est = 1;
            return;
        }
        else if(ch == '>'){
            printf("OP_DIFERENTE \n");
            est = 1;
            return;
        }
        else{
            printf("OP_MENOR \n");
            est = 1;
            return;
        }
    case 7:
        if(ch >= '0' && ch <= '9'){
            printf("SOMA \n");
            est = 2;
            anali_lex(ch);
            break;
        }
    case 8:
        if(ch >= '0' && ch <= '9'){
            printf("SUBTRACAO \n");
            est = 2;
            anali_lex(ch);
            break;
        }
    }
    return;
    
}
