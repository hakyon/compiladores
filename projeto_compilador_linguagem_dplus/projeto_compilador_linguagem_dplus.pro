TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

SOURCES += main.c

HEADERS += include/analise_lexica.h \
    include/analise_semantica.h \
    include/tipos.h \
    include/analise_sintatica.h \
    include/geracao_mepa.h

DISTFILES += \
    instrução/regras.txt \
    codigo.txt
