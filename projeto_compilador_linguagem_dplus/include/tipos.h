#ifndef TIPOS_H
#define TIPOS_H

#include <string.h>
#include <stdlib.h>

#define ATRIBUICAO      "SOMA"
#define SUBTRACAO       "SUBTRAI"
#define MULTIPLICACAO   "MULTIPLICA"
#define DIVISAO         "DIVISAO"
#define PORCENTUAL      "PORCENTUAL"
#define PONTO           "PONTO"
#define VIRGULA         "SEPARADOR"
#define DOIS_PONTOS     "ALTERNATIVA"
#define INTEIRO         "INT"
#define DECIMAL         "FLOAT"
#define MENOR_QUE       "MENOR_QUE"
#define MAIOR_QUE       "MAIOR_QUE"
#define MAIOR_IGUAL_QUE "MAIOR_IGUAL_QUE"
#define MENOR_IGUAL_QUE "MENOR_IGUAL_QUE"
#define NEGACAO         "NEGA"
#define OPERADOR_END    "OPERADOR_ENDERECO"
#define RESTO_DIV       "RESTANTE_DIVISAO"
#define RESTO_ATR       "RESTO_DIV_ATRIBUICAO"
#define AND             "E"
#define OR              "OU"
#define DIFERENTE       "DIFERENTE"
#define SIFRAO          "SIFRAO"
#define ASPAS_SIMPLES   "ASPAS_SIMPLES"
#define ASPAS_DUPLAS    "ASPAS_DUPLAS"
#define CHECK           "CHECK_EXPRESSAO"
#define IGUAL           "IGUAL"
#define IGUALDADE       "IGUALDADE"
#define INCREMENTA_1    "INCREMENTA_1"
#define DECREMENTA_1    "DECREMENTA_1"
#define SOMA_ATRIBUI    "SOMA_ATRIBUI"
#define SUB_ATRIBUI     "SUB_ATRIBUI"
#define MULTI_ATRIBUI   "MULTI_ATRIBUI"
#define DIVI_ATRIBUI    "DIVIDI_ATRIBUI"
#define APONTA_DIREITA  "APONTA_DIREITA"
#define APONTA_ESQUERDA "APONTA_ESQUERDA"
#define UNDERLINE       "UNDERLINE"
#define CHAR            "CARACTER"
#define STRING          "STRING"
#define BOOLEANO        "BOOLEANO"
#define ABRE_PARENTESE  "ABRE_PARENTESE"
#define FECHA_PARENTESE "FECHA_PARENTESE"
#define ABRE_COLCHETE   "ABRE_COLCHETE"
#define FECHA_COLCHETE  "FECHA_COLCHETE"
#define ABRE_CHAVES     "ABRE_CHAVES"
#define FECHA_CHAVES    "FECHA_CHAVES"
#define ENDERECO        "ENDERECO_MEMORIA"
#define QUEBRA_LINHA    "QUEBRA_LINHA"
#define CARACTER        "CARACTER"
#define STRING          "STRING"

// Palavras reservadas

#define CONDICIONAL  "if"
#define REPETICAO    "for"
#define REPETICAO2   "while"
#define FAZER        "do"
#define SENAO        "else"
#define SENAOSE      "else if"
#define RETORNO      "return"
#define BOLEANO      "bool"
#define PARADA       "break"
#define CONTINUA     "continue"
#define DIRECIONADOR "goto"
#define VERDADEIRO   "true"
#define FALSO        "false"
#define VAZIO        "void"
#define TIPO_INTEIRO "int"
#define TIPO_REAL    "real"
#define TIPO_CARACTER "char"

//Tokens para palavras reservadas

#define IF           "IF"
#define FOR          "FOR"
#define WHILE        "WHILE"
#define DO           "DO"
#define ELSE         "ELSE"
#define ELSEIF       "ELSE IF"
#define RETURN       "RETURN"
#define BOOLEAN      "BOOL"
#define BREAK        "BREAK"
#define CONTINUE     "CONTINUE"
#define GOTO         "GOTO"
#define TRUE         "TRUE"
#define FALSE        "FALSE"
#define VOID         "VOID"
#define TYPE_INT     "TIPO_INT"
#define TYPE_FLOAT   "TIPO_FLOAT"
#define TYPE_CHAR    "TIPO_CHAR"
#define VAR          "VAR"



// Comentarios

#define ABRINDO_COMENTARIO     "/*"
#define FECHANDO_COMENTARIO    "*/"
#define COMENTARIO_LINHA       "//"
#define BLOCO_COMENTARIO       "BLOCO COMENTADO"


struct relatorio_lexico {
    int estado;
    char *token;
    char lexema;
};

struct relatorio_sintatico {
    char *nome;
    char *tipo;
    char *categoria;
    int tamanho;
    char *escopo;
    char *dados;
};


char *buffer = NULL;

extern struct relatorio_lexico relatorio_analise_lexixa;
extern struct relatorio_sintatico relatorio_analise_sintatica;


// utiliario

char *append(const char *orig, char c)
{
    size_t sz = strlen(orig);
    char *str = malloc(sz + 2);
    strcpy(str, orig);
    str[sz] = c;
    str[sz + 1] = '\0';
    return str;
}

char *ler_arquivo()
{
    size_t size = 0;

    FILE *fp = fopen("codigo.txt", "r");

    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    rewind(fp);

    buffer = malloc((size + 1) * sizeof(*buffer));
    fread(buffer, size, 1, fp);
    buffer[size] = '\0';

    // n_simbolos = strlen(buffer);


    return buffer;
}


#endif // TIPOS_H
