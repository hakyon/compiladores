/*******************************************************************/
/*Autor: Alan Azevedo Bancks                                       */
/*                                                                 */
/*Projeto:Analisador Sintatico para uma Compilador D+              */
/*Data:30/04/2018                                                  */
/*******************************************************************/

#include <stdio.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <ctype.h>
#include "tipos.h"

void imprimindo_tabela_simbolos(struct relatorio_sintatico **relatorio_analise_sintatica);

void analisador_sintatico (char* expressao,struct relatorio_sintatico **relatorio_analise_sintatica ){

    int i,j,k=0,count,inc=0,n;
    char open[30],ch,chh,o[30];
    char operacao[30]={'+','-','*','/','%',
                       '?',':','!','&','.',
                       '->','<','>','==',
                       '!=','<=','>=','=',
                       '+=','-=','*=','/=',
                       '%=','++','&&','||'};

    // por enquanto so validando operandos com operador

    n=strlen(expressao);
    for(i=0;i<n;i++) // processa tamanho da expressao
    {
        ch=tolower(expressao[i]);
        for(j=0;j<9;j++)
        {
            if(ch==operacao[j]) // verifica o operador
            {
                open[k]=i; // captura primeiro operando
                o[k]=ch; // armazena operacao
                k++;     // prepara o processo ao proximo simbolo
            }
        }
    }

    // pega operador de verifico quantidade de operandos para saber se esta valido
    // +22
    // -34
    // ||22

    for(i=0;i<k;i++) // processa pelo numero de operacoes
    {
        count=open[i];
        ch=tolower(expressao[count-1]); // pega primeiro operando
        if(isalpha(ch)){
            relatorio_analise_sintatica[i]->tipo = "VARIAVEL";
            relatorio_analise_sintatica[i]->escopo = "GLOBAL";
            relatorio_analise_sintatica[i]->categoria ="VAR";
            relatorio_analise_sintatica[i]->nome = ch;
            // aqui precisa verificar o conteudo da variavel para colocar os dados
            // sabendo o conteudo podemos dizer o seu tamanho

        }
        else if(isdigit(ch)){
            relatorio_analise_sintatica[i]->tipo = "CONSTANTE";
            relatorio_analise_sintatica[i]->escopo = "GLOBAL";
            relatorio_analise_sintatica[i]->categoria ="VAR";
            relatorio_analise_sintatica[i]->dados = ch;
            // apesar de ser temporario, quando for numerico o valor passamos a considerar uma constante


        }
        chh=tolower(expressao[count+1]);  // segundo operando
        if(isalpha(chh)){
//            relatorio_analise_sintatica[i+1]->nome = ch;
//            relatorio_analise_sintatica[i+1]->tipo = "VARIAVEL";
//            relatorio_analise_sintatica[i+1]->escopo = "GLOBAL";
//            relatorio_analise_sintatica[i+1]->categoria ="VAR";


        }
        else if(isdigit(chh)){
//            relatorio_analise_sintatica[i+1]->tipo = "CONSTANTE";
//            relatorio_analise_sintatica[i+1]->escopo = "GLOBAL";
//            relatorio_analise_sintatica[i+1]->categoria ="VAR";
//            relatorio_analise_sintatica[i+1]->dados = chh;



        }
        if(isalpha(ch)&&isalpha(chh)||isdigit(chh))  // verifica se eh um caracter alfa numerico ou digito
            ++inc;
    }

    if(k==inc)
        printf("Concluida analise sintatica sem erros \n");
    else
        printf("Concluida analise sintatica com erros \n");

    //imprimindo_tabela_simbolos(relatorio_analise_sintatica);

    return;
}

void imprimindo_tabela_simbolos(struct relatorio_sintatico **relatorio_analise_sintatica){

    // arrumando tabela de simbolos

    printf("Tabela de simbolos: \n");
    printf("Nome |Tipo |Tamanho |Escopo |Categoria |Dados   | \n");
    for(int i=0;i<=sizeof(relatorio_analise_sintatica);i++){
        if(relatorio_analise_sintatica[i]->tamanho > 99){ // limpando sujeira
           // relatorio_analise_sintatica[i]->tamanho = 0;
        }
        if(relatorio_analise_sintatica[i]->nome == 0x10){
            relatorio_analise_sintatica[i]->nome = "SEM NOME";
        }
        if(relatorio_analise_sintatica[i]->dados == 0x31){
            relatorio_analise_sintatica[i]->dados = "";
        }

        printf("%s |%s |%d |%s |%s |%s  |\n" ,
                 relatorio_analise_sintatica[i]->nome,
                 relatorio_analise_sintatica[i]->tipo,
                 relatorio_analise_sintatica[i]->tamanho,
                 relatorio_analise_sintatica[i]->escopo,
                 relatorio_analise_sintatica[i]->categoria,
                 relatorio_analise_sintatica[i]->dados
               );
    }
}
