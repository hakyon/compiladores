/*******************************************************************/
/*Autor: Alan Azevedo Bancks                                       */
/*                                                                 */
/*Projeto:Analisador Semantico para uma Compilador D+              */
/*Data:03/06/2018                                                  */
/*******************************************************************/

#ifndef ANALISE_SEMANTICA_H
#define ANALISE_SEMANTICA_H

// codigo de exemplo, sera modificado


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "tipos.h"

// void delete (struct node);

#define memoryAlloc() (struct node *)malloc(sizeof(struct node));

struct prod
{
    char left;
    char right[10];
    int end;
};

struct queue
{
    struct node *nodeptr;
    struct queue *next;
};

struct node
{
    char c;
    struct node *lchild;
    struct node *rchild;
    struct node *opr;
    int visited;
    int first;
    int inh, syn, val;
};

struct regra_semantica
{
    char c;
    char *action;
    char *end;
};

void createlistbfs(struct node *);
int arvore_pesquisa_profunda(struct node *, struct regra_semantica []);
void busca(struct regra_semantica [], char, struct node *);
void memberValueSet(struct node **, struct prod[], int, int);
void imprimindo_gramatica(struct prod []);
void digitAlphaNode(int *, int *, int *, struct prod [], struct node **, struct node **, struct node **, struct node *, char);
void openCloseBracketNodes(struct prod gramatica[], struct node *f, struct node **ob, struct node **cb);
struct node * bracketOperationCall (struct prod [], char [], int *, int *);
struct node *mknode(char *, struct prod [], int);

void delete (struct node **root)
{
    if ((*root)->lchild)
        delete(&(*root)->lchild);
    if ((*root)->opr)
        delete(&(*root)->opr);
    if ((*root)->rchild)
        delete(&(*root)->rchild);

    free(*root);
    *root = NULL;
}



void analisador_semantico (char* expressao)
{
    expressao[strlen(expressao)-1] = 0;
    /*gramaticaatica que sera criada para o parsiamento*/
    struct prod gramatica[6] = {'E', "TP", 0,
                           'P', "+TP|-TP", 1,
                           'T', "FQ", 0,
                           'Q', "*FQ|/FQ", 1,
                           'F', "d", 0,
                           'F', "(E)", 0};

    /*Regra semantica para uma calculadora*/
    struct regra_semantica regras[11] = {'d', "F.valor = id.lexico","",
                                     'F', "Q.inh = F.valor","",
                                     '*', "Q1.inh = Q.inh * F.valor","",
                                     '/', "Q1.inh = Q.inh / F.valor","",
                                     '+', "P1.inh = P.inh + T.valor","",
                                     '-', "P1.inh = P.inh - T.valor","",
                                     'T', "P.inh = T.valor","",
                                     'Q', "Q.simbolo = Q.inh","T.val = Q.simbolo",
                                     'P', "P.simbolo = P.inh","E.val = P.simbolo",
                                     'E', "L.valor = E.valor","",
                                     ')', "F.valor = E.valor", ""};
    struct node *arvore = NULL;

    printf_s("\n");

    if (arvore)
        delete(&arvore);
    arvore = mknode(expressao, gramatica, 0);




    return;
}

void imprimindo_gramatica(struct prod gramatica[])
{
    printf("Imprimindo gramatica \n");
    int i;
    for(i = 0; i < 6; i++)
    {
        printf("\n%c -> %s", gramatica[i].left, gramatica[i].right);
        if (gramatica[i].end)
        {
            printf("|e");
        }
    }
    printf("\n");
}

struct node* mknode(char *expr, struct prod gramatica[], int flag)
{
    struct node *t, *p, *f, *q, *id, *e, *temp, *ob, *cb;
    /*
     * The pointers will point to their respective nodes
     * t will point to nodes in imprimindo_gramatica that uses T
     * p will point to nodes in imprimindo_gramatica that uses P
     * f will point to nodes in imprimindo_gramatica that uses F
     * and so on..
     */
    t = p = f = q = id = e = temp = ob = cb = NULL;
    int countOfOperands = 0;
    int digitFlag = 0;
    int operatorFlag = 0;
    /*
     * The purpose of operatorFlag and digitFlag is to prevent
     * multiple operands or operators consecutively which the
     * project doesn't support
     */

    e = memoryAlloc();
    e->c = gramatica[0].left;
    e->first = 0;
    if (flag == 0)
    {
        e->first = 1;
        flag = 1;
    }

    e->opr = e->rchild = e->lchild = NULL;

    p = memoryAlloc();
    p->c = gramatica[0].right[1];
    e->rchild = p;
    p->opr = p->lchild = p->rchild = NULL;
    p->first = 1;

    t = memoryAlloc();
    t->c = gramatica[0].right[0];
    e->lchild = t;
    t->opr = t->lchild = t->rchild = NULL;

    printf("%c -> %c%c\n", e->c, t->c, p->c);

    int i, j;
    char ch;
    for(i = 0; i < strlen(expr); i++)
    {
        if(isdigit(expr[i]) || isalpha(expr[i]))
        countOfOperands++;
    }
    i = 0;
    ch = expr[i++];
    if (ch == '(')
    {
        f = memoryAlloc();
        t->lchild = f;
        memberValueSet(&f, gramatica, 2, 0);

        q = memoryAlloc();
        t->rchild = q;
        q->first = 1;
        memberValueSet(&q, gramatica, 2, 1);

        printf("%c -> %c%c\n", t->c, f->c, q->c);
        openCloseBracketNodes(gramatica, f, &ob, &cb);
        printf("%c -> %c%c%c\n", f->c, ob->c, 'E', cb->c);
        f->opr = bracketOperationCall(gramatica, expr, &i, &countOfOperands);
        ch = expr[i++];
    }
    while (countOfOperands)
    {
        if (ch != '+' && ch != '-' && ch != '*' && ch != '/' && !isdigit(ch) && !isalpha(ch))
        {
            printf(" Caracter invalido: %c\n Tente novamente \n", ch);
            return NULL;
        }
        if (isdigit(ch) || isalpha(ch))
        {
            digitAlphaNode(&digitFlag, &operatorFlag, &countOfOperands, gramatica, &f, &id, &q, t, ch);
            ch = expr[i++];
        }
        else if (ch == '+' || ch == '-')
        {
            if (!operatorFlag)
            {
                digitFlag = 0;
                operatorFlag = 1;
            }
            else if (operatorFlag)
            {
                printf("Uso invalido de +/-\n Tente novamente\n");
                return NULL;
            }

            t = memoryAlloc();
            p->lchild = t;
            memberValueSet(&t, gramatica, 1, 1);

            id = memoryAlloc();
            p->opr = id;
            id->opr = id->lchild = id->rchild = NULL;
            id->c = ch;

            temp = p;
            p = memoryAlloc();
            temp->rchild = p;
            p->first = 0;
            memberValueSet(&p, gramatica, 1, 2);

            if (q->rchild == NULL && q->lchild == NULL && q->opr == NULL)
                  printf("%c -> ϵ\n", q->c);

            printf("%c -> %c%c%c\n", temp->c, id->c, t->c, p->c);

            ch = expr[i++];
            if (isdigit(ch) || isalpha(ch))
            {
                digitAlphaNode(&digitFlag, &operatorFlag, &countOfOperands, gramatica, &f, &id, &q, t, ch);
                ch = expr[i++];
            }
            else if (ch == ')')
            {
                printf("Uso incorreto de ) depois de + ou -\n Tente novamente \n");
                return NULL;
            }
            else if (ch == '(')
            {
                f = memoryAlloc();
                t->lchild = f;
                memberValueSet(&f, gramatica, 2, 0);

                q = memoryAlloc();
                t->rchild = q;
                q->first = 1;
                memberValueSet(&q, gramatica, 2, 1);
                printf("%c -> %c%c\n", t->c, f->c, q->c);

                openCloseBracketNodes(gramatica, f, &ob, &cb);

                printf("%c -> %c%c%c\n", f->c, ob->c, 'E', cb->c);

                f->opr = bracketOperationCall(gramatica, expr, &i, &countOfOperands);
                ch = expr[i++];
            }
         }
         else if (ch == '*' || ch == '/')
         {
             if (!operatorFlag)
             {
                 digitFlag = 0;
                 operatorFlag = 1;
             }
             else if (operatorFlag)
             {
                 printf("Invalido */-\n Tente novamente \n");
                 return NULL;
             }
             f = memoryAlloc();
             q->lchild = f;
             memberValueSet(&f, gramatica, 3, 1);

             id = memoryAlloc();
             q->opr = id;
             id->c = ch;
             id->val = 0;
             id->rchild = id->lchild = id->opr = NULL;

             temp = q;
             q = memoryAlloc();
             q->first = 0;
             temp->rchild = q;
             memberValueSet(&q, gramatica, 3, 2);

             printf("%c -> %c%c%c\n", temp->c, id->c, f->c, q->c);

             ch = expr[i++];
             if (isdigit(ch) || isalpha(ch))
             {
                 /*digitos e caractes alfanumericos*/
                 if (!digitFlag)
                 {
                     digitFlag = 1;
                     operatorFlag = 0;
                 }
                 else if (digitFlag)
                 {
                    printf("Uso de multiplos digitos \n Tente novamente\n");
                    return NULL;
                 }
                 id = memoryAlloc();
                 f->opr = id;
                 id->c = ch;
                 id->val = ch - '0';
                 id->opr = id->lchild = id->rchild = NULL;

                 countOfOperands--;
                 printf("%c -> %c\n", f->c, id->c);
                 ch = expr[i++];
             }
             else if (ch == ')')
             {
                 printf("Uso incorreto de ) antes + ou - \n Tente novamente\n");
                 return NULL;
             }
             else if (ch == '(')
             {
                 openCloseBracketNodes(gramatica, f, &ob, &cb);

                 printf("%c -> %c%c%c\n", f->c, ob->c, 'E', cb->c);

                 f->opr = bracketOperationCall(gramatica, expr, &i, &countOfOperands);
                 ch = expr[i++];
            }
         }
     }
     if (strlen(expr) % 2 == 0)
     {
          printf("Concluida analise semantica com erros\n");
          return NULL;
     }

     printf("%c -> ϵ\n", q->c);
     printf("%c -> ϵ\n", p->c);
     printf("Concluida analise semantica sem erros\n");


     return e;
}

void digitAlphaNode(int *digitFlag, int *operatorFlag, int *countOfOperands, struct prod gramatica[],
                    struct node **f, struct node **id, struct node **q, struct node *t,
                    char ch)
{

    if (!(*digitFlag))
    {
        (*digitFlag) = 1;
        (*operatorFlag) = 0;
    }
    else if ((*digitFlag))
    {
        printf("Uso de multiplos digitos \n Tenta novamente \n");
        return;
    }
    (*countOfOperands)--;
    *f = memoryAlloc();
    t->lchild = *f;
    memberValueSet(&(*f), gramatica, 2, 0);

    *id = memoryAlloc();
    (*f)->opr = *id;
    (*id)->opr = (*id)->lchild = (*id)->rchild = NULL;
    (*id)->c = ch;
    (*id)->val = ch - '0';

    (*q) = memoryAlloc();
    t->rchild = (*q);
    (*q)->first = 1;
    memberValueSet(&(*q), gramatica, 2, 1);

    printf("%c -> %c%c\n", t->c, (*f)->c, (*q)->c);
    printf("%c -> %c\n", (*f)->c, (*id)->c);
}

void openCloseBracketNodes(struct prod gramatica[], struct node *f, struct node **ob, struct node **cb)
{
    (*ob) = memoryAlloc();
    f->lchild = (*ob);
    memberValueSet(&(*ob), gramatica, 5, 0);

    (*cb) = memoryAlloc();
    f->rchild = (*cb);
    memberValueSet(&(*cb), gramatica, 5, 2);
}

struct node * bracketOperationCall (struct prod gramatica[], char expr[], int *i, int *countOfOperands)
{
    int j = *i, k = 0, ob_count = 0;
    char b_expr[20];
    int prev_count = *countOfOperands;

    while (expr[j] != ')' || ob_count)
    {
        b_expr[k++] = expr[j++];
        if(b_expr[k-1] == '(')
            ob_count++;
        else if(b_expr[k-1] == ')')
            ob_count--;
        else if(isdigit(b_expr[k-1]) || isalpha(b_expr[k-1]))
            (*countOfOperands)--;
    }
    if (ob_count || prev_count == *countOfOperands)
    {
         printf("Invalido ) ou conteudo dentro de () perdido \n Tente novamente \n");
         return NULL;
    }
    b_expr[k] = '\0';
    (*i) = ++j;

    return mknode(b_expr, gramatica, 1);
}

void memberValueSet(struct node **tempNode, struct prod gramatica[], int m, int n)
{
    (*tempNode)->c = gramatica[m].right[n];
    (*tempNode)->rchild = (*tempNode)->lchild = (*tempNode)->opr = NULL;
    (*tempNode)->val = 0;
}

int arvore_pesquisa_profunda(struct node *root, struct regra_semantica regras[])
{
    char *str;
    root->visited = 1;

    if (root->lchild)
        if(root->lchild->visited == 0)
            arvore_pesquisa_profunda(root->lchild, regras);

    if (root->opr)
        if(root->opr->visited == 0)
            arvore_pesquisa_profunda(root->opr, regras);

    if (root->rchild)
        if(root->rchild->visited == 0)
            arvore_pesquisa_profunda(root->rchild, regras);

    printf("[%c]\n", root->c);
    busca(regras, root->c, root);
    root->visited = 0;
}

void busca(struct regra_semantica regras[], char a, struct node *root)
{
    int i;
    for (i = 0; i < 11; i++)
    {
        if (isdigit(a))
            a = 'd';
        else if (islower(a))
            a = 'd';
        if (regras[i].c == 'E' && !root->first)
            return;
        if (regras[i].c == a)
        {
            printf("%s\n", regras[i].action);
            if (root->first == 1)
                printf("%s\n", regras[i].end);
            printf("\n");
            return;
        }
    }
}

void createlistbfs(struct node *root)
{

    struct queue *qhead, *qrear, *qtemp, *qrelease;

    if (root == NULL)
    {
        return;
    }
    qhead = (struct queue *)malloc(sizeof(struct queue));
    qhead->nodeptr = root;
    qhead->next = NULL;
    qrear = qhead;
    while (qhead != NULL)
    {
        printf("%c  ", qhead->nodeptr->c);
        if (qhead->nodeptr->opr != NULL)
        {
            qtemp = (struct queue *)malloc(sizeof(struct queue));
            qtemp->nodeptr = qhead->nodeptr->opr;
            qtemp->next = NULL;
            qrear->next = qtemp;
            qrear = qtemp;
        }
        if (qhead->nodeptr->lchild != NULL)
        {
            qtemp = (struct queue *)malloc(sizeof(struct queue));
            qtemp->nodeptr = qhead->nodeptr->lchild;
            qtemp->next = NULL;
            qrear->next = qtemp;
            qrear = qtemp;
        }
        if (qhead->nodeptr->rchild != NULL)
        {
            qtemp = (struct queue *)malloc(sizeof(struct queue));
            qtemp->nodeptr = qhead->nodeptr->rchild;
            qtemp->next = NULL;
            qrear->next = qtemp;
            qrear = qtemp;
        }
        qrelease = qhead;
        qhead = qhead->next;
        free(qrelease);
    }
}



#endif // ANALISE_SEMANTICA_H
