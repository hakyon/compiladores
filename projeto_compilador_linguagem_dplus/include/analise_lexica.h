#ifndef ANALISE_LEXICA_H
#define ANALISE_LEXICA_H

/*******************************************************************/
/*Autor: Alan Azevedo Bancks                                       */
/*                                                                 */
/*Projeto:Analisador Lexico para uma Compilador D+                 */
/*Data:23/03/2018                                                  */
/*******************************************************************/

#include "tipos.h"


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void anali_lex(char,struct relatorio_lexico **relatorio_analise_lexica);
char prox_char();
int est = 0;
int posicao_expresao;
int posicao;
char *palavra_reservada_temp = "" ;
char simbolo_anterior;
int m = 0;
int erros_lexicos = 0;


char *expressao_em_analise;
int inicio_palavra_reservada = 0;
int inicio_string = 0 ;
int inicio_comentario = 0;
int n_aspas = 0; // numero par de aspas duplas significa possiveis strings validas

void analisador_lexico (char* expressao,struct relatorio_lexico **relatorio_analise_lexica ){

    // limpando lixo, depois arrumo isso
    expressao[strlen(expressao)-1] = 0;


    printf("Iniciando analise da expressao :\n %s \n",expressao);
    expressao_em_analise = expressao;
    char ch;
    size_t tam_expressao = strlen(expressao);

     for(int i=0;i<=tam_expressao;i++){
        posicao = posicao_expresao = i;
        ch = expressao[i];
        //printf("%c \n",ch);
        anali_lex(ch,&relatorio_analise_lexica);
    }
    //anali_lex(ch,&relatorio_analise_lexica);

     if(erros_lexicos == 0){
         printf("\nConcluida analise lexica sem erros \n");
     }
     else{
         printf("\nConcluida analise lexica com erros \n");
     }
    return;
}

//Erros

void erro(char simbolo){
    //printf("Simbolo descartado %c \n",simbolo);
    //erros_lexicos++;
}
void erro_caracter(){
    printf("FALHA - Caracter mal formado \n");
    erros_lexicos++;
    exit(-1);
}
void erro_string(){
    printf("FALHA - String mal formada \n");
    erros_lexicos++;
    exit(-1);
}

void erro_simbolo_desconhecido(char simbolo){
    printf("FALHA - Simbolo desconhecido: %c \n",simbolo);
    erros_lexicos++;
    exit(-1);
}






//
void anali_lex(char ch ,struct relatorio_lexico **relatorio_analise_lexica ) {

    //printf("Estado:%d \n",est);

    switch(est){
    case 0:
        if (ch >= '0' && ch <= '9'){
            est = 1;
            return;
        }
        else if (ch == '+'){
            est = 6;
            break;
        }
        else if (ch == '-'){
            est = 7;
            return;
        }
        else if (ch == '/'){
            est = 8;
            return;
        }
        else if (ch == '*'){
            est = 9;
            return;
        }
        else if (ch == '\s' || ch == '\t' || ch == '\n'){
            //printf("FIM \n");
            est = 0;
            return;
        }
        else if (ch == '('){
            est = 10;
            return;
        }
        else if (ch == ')'){
            est = 11;
            return;
        }
        else if (ch == '['){
            est = 12;
            return;
        }
        else if (ch == ']'){
            est = 13;
            return;
        }
        else if (ch == '{'){
            est = 14;
            return;
        }
        else if (ch == '}'){
            est = 15;
            return;
        }
        else if (ch == '='){
            est = 4;
            break;
        }
        else if (ch == '<'){
            est = 5;
            break;
        }
        else if (ch == '>'){
            est = 18;
            break;
        }
        else if (ch == '%'){
            est = 20;
            break;
        }
        else if (ch == '!'){
            est = 29;
            break;
        }
        else if (ch == '?'){
            est = 31;
            break;
        }
        else if (ch == ':'){
            est = 32;
            break;
        }
        else if (ch == '&'){
            est = 33;
            break;
        }
        else if (ch == ','){
            est = 34;
            break;
        }
        else if (ch == ';'){
            est = 35;
            break;
        }
        else if (ch == '|'){
            est = 37;
            break;
        }
        else if (ch == '$'){
            est = 39;
            break;
        }
        else if (ch == '_'){
            est = 40;
            break;
        }
        else if (ch == '\''){
            est = 41;
            break;
        }
        else if (ch == '\"'){
            est = 42;

            while(expressao_em_analise != NULL)
            {
                expressao_em_analise = strchr(expressao_em_analise,'\"');
                if( expressao_em_analise ) {
                    expressao_em_analise++;
                    n_aspas++;
                }
            }
            break;
        }
        else if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)){ // valida a-zA-Z
            est = 45; //estado para iniciar validação para palavras reservadas
            palavra_reservada_temp = append(palavra_reservada_temp,ch);
            inicio_palavra_reservada++;
            simbolo_anterior = ch;
            break;
        }
        else{
            erro(ch);
            return;
        }


    case 1:
        if(ch >= '0' && ch <= '9'){
            est = 1;
            return;
        }
        else if(ch == '.'){
            est = 2;
            return;
        }
        else{
            printf("%s\n",INTEIRO);
            m=m+1;
//            relatorio_analise_lexica[m]->estado = est;
//            relatorio_analise_lexica[m]->lexema = expressao_em_analise[posicao_expresao-1];
//            relatorio_analise_lexica[m]->token = INTEIRO;
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 2:
        if(ch >= '0' && ch <= '9'){
            est = 3;
            return;
        }
        else{
            erro(ch);
            return;
        }
    case 3:
        if(ch >= '0' && ch <= '9'){
            est = 3;
            return;
        }
        else{
            printf("%s\n",DECIMAL);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 4:
        if(ch == '='){
            est = 16;
            return;
        }
        else{
            printf("%s\n",IGUAL);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 5:
        if(ch == '='){
            est = 17;
            return;
        }
        else{
            printf("%s\n",MENOR_QUE);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 6:
        if(ch == '='){
            est = 21;
            return;
        }
        else if(ch == '+'){
            est = 26;
            return;
        }
        else{
            printf("%s\n",ATRIBUICAO);
            m=m+1;
//            relatorio_analise_lexica[m]->estado = est;
//            relatorio_analise_lexica[m]->lexema = expressao_em_analise[posicao_expresao-1];
//            relatorio_analise_lexica[m]->token = ATRIBUICAO;
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);

            break;
        }
    case 7:
        if(ch == '='){
            est = 22;
            return;
        }
        else if(ch == '-'){
            est = 27;
            return;
        }
        else if(ch == '>'){
            est = 28;
            return;
        }
        else{
            printf("%s\n",SUBTRACAO);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 8:
        if(ch == '='){
            est = 23;
            return;
        }
        if(ch == '*'){
            est = 43;
            return;
        }
        if(ch == '/'){
            est = 44;
            return;
        }
        else{
            printf("%s\n",DIVISAO);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 9:
        if(ch == '='){
            est = 24;
            return;
        }
        else{
            printf("%s\n",MULTIPLICACAO);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 10:
        printf("%s\n",ABRE_PARENTESE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 11:
        printf("%s\n",FECHA_PARENTESE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 12:
        printf("%s\n",ABRE_COLCHETE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 13:
        printf("%s\n",FECHA_COLCHETE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 14:
        printf("%s\n",ABRE_CHAVES);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 15:
        printf("%s\n",FECHA_CHAVES);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 16:
        printf("%s\n",IGUALDADE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 17:
        printf("%s\n",MENOR_IGUAL_QUE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 18:
        if(ch == '='){
            est = 19;
            return;
        }
        else{
            printf("%s\n",MAIOR_QUE);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 19:
        printf("%s\n",MAIOR_IGUAL_QUE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 20:
        if(ch == '='){
            est = 25;
            return;
        }
        else{
            printf("%s\n",RESTO_DIV);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 21:
        printf("%s\n",SOMA_ATRIBUI);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 22:
        printf("%s\n",SUB_ATRIBUI);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 23:
        printf("%s\n",DIVI_ATRIBUI);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 24:
        printf("%s\n",MULTI_ATRIBUI);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 25:
        printf("%s\n",RESTO_ATR );
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 26:
        printf("%s\n",INCREMENTA_1 );
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 27:
        printf("%s\n",DECREMENTA_1 );
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 28:
        printf("%s\n",APONTA_DIREITA);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 29:
        if(ch == '='){
           est = 30;
           return;
        }
        else{
            printf("%s\n",NEGACAO);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 30:
        printf("%s\n",DIFERENTE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 31:
        printf("%s\n",CHECK);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 32:
        printf("%s\n",DOIS_PONTOS);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 33:
        if(ch == '&'){
            est = 36;
            return;
        }
        else{
            printf("%s\n",ENDERECO);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
    case 34:
        printf("%s\n",VIRGULA);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 35:
        printf("%s\n",QUEBRA_LINHA);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 36:
        printf("%s\n",AND);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;

    case 37:
        if(ch == '|'){
            est = 38;
            return;
        }
        else{
            erro(ch);
            return;
        }
    case 38:
        printf("%s\n",OR);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;     
    case 39:
        if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122) || (ch >= 48 && ch <= 57)){ // valida a-zA-Z 0-9
            return ;
        }
        else{
            printf("%s\n",VAR);
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            return;
        }
        break;
    case 40:
        printf("%s\n",UNDERLINE);
        est = 0;
        anali_lex(ch,&relatorio_analise_lexica);
        break;
    case 41:
        if(ch  != '\'' && expressao_em_analise[posicao_expresao+1] == '\''){
          return;
        }
        else if(ch  != '\''){
           erro_caracter();
           return;
        }
        if(ch  == '\''){
           printf("%s\n",CARACTER);
           est = 0;
           break;
        }
    case 42:
        if(ch != '\"' && inicio_string == 0){
            inicio_string = 1;


            if(n_aspas % 2 == 0 ){
                return;
            }
            else{
                erro_string();
                return;
            }


        }
        if(inicio_string == 1 && ch == '\n'){
           erro_string();
           return;
        }
        if(ch == '\"' && inicio_string == 1){
            est = 0;
            printf("%s\n",STRING);
            inicio_string = 0;
            return;

        }
        break;
    case 43:
        if(ch != '*' && inicio_comentario == 0){
            inicio_comentario = 1;
            return;
        }
        if(ch == '*' && inicio_comentario == 1){
            inicio_comentario = 2;
            return;
        }
        if(ch == '/' && inicio_comentario == 2){
            inicio_comentario = 0;
            printf("%s\n",BLOCO_COMENTARIO);
            est = 0;
            return;
        }
        if ((ch >= 60 && ch <= 90) || (ch >= 97 && ch <= 122)){ //valição de qualquer palavra dentro do comentario
            return;
        }
        if((ch == '\s' || ch == '\t' || ch == '\n' || ch == '\0' || ch == 32)){ //validação de qualquer tipo de espaço no comentario
            return;
        }
    case 44:
        if(ch != ';' && inicio_comentario == 0){
            inicio_comentario = 1;
            return;
        }
        if(ch == ';' && inicio_comentario == 1){
            inicio_comentario = 0;
            printf("%s\n",BLOCO_COMENTARIO);
            est = 0;
            return;
        }
     case 45:
        if ((ch >= 60 && ch <= 90) || (ch >= 97 && ch <= 122) ){
            palavra_reservada_temp = append(palavra_reservada_temp,ch);
            inicio_palavra_reservada++;

            if (strcmp(palavra_reservada_temp,CONDICIONAL) == 0){
                est = 46;
                break;
            }
            else if (strcmp(palavra_reservada_temp,REPETICAO) == 0){
                est = 47;
                break;
            }
            else if (strcmp(palavra_reservada_temp,REPETICAO2) == 0){
                est = 48;
                break;
            }
            else if (strcmp(palavra_reservada_temp,FAZER) == 0){
                est = 49;
                break;
            }
            else if (strcmp(palavra_reservada_temp,SENAO) == 0){
                est = 50;
                break;
            }
            else if (strcmp(palavra_reservada_temp,RETORNO) == 0){
                est = 51;
                break;
            }
            else if (strcmp(palavra_reservada_temp,PARADA) == 0){
                est = 52;
                break;
            }
            else if (strcmp(palavra_reservada_temp,CONTINUA) == 0){
                est = 53;
                break;
            }
            else if (strcmp(palavra_reservada_temp,DIRECIONADOR) == 0){
                est = 54;
                break;
            }
            else if (strcmp(palavra_reservada_temp,BOLEANO) == 0){
                est = 55;
                break;
            }
            else if (strcmp(palavra_reservada_temp,VAZIO) == 0){
                est = 56;
                break;
            }
            else if (strcmp(palavra_reservada_temp,VERDADEIRO) == 0){
                est = 57;
                break;
            }
            else if (strcmp(palavra_reservada_temp,FALSO) == 0){
                est = 58;
                break;
            }
            else if (strcmp(palavra_reservada_temp,TIPO_INTEIRO) == 0){
                est = 59;
                break;
            }
            else if (strcmp(palavra_reservada_temp,TIPO_REAL) == 0){
                est = 60;
                break;
            }
            else if (strcmp(palavra_reservada_temp,TIPO_CARACTER) == 0){
                est = 61;
                break;
            }
            else{
                return;
            }
            return;

        }
        else if(ch == 32 || ch == '\n' || ch == 0){
            est = 0;
            palavra_reservada_temp = "";
            return;
        }
        else{
            est = 0;
            anali_lex(ch,&relatorio_analise_lexica);
            break;
        }
     case 46:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",IF);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
        return;
    case 47:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",FOR);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
        return;
    case 48:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",WHILE);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
        return;
    case 49:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",DO);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 50:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",ELSE);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 51:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",RETURN);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 52:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",BREAK);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 53:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",CONTINUE);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 54:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",GOTO);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 55:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",BOOLEAN);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 56:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",VOID);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 57:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",TRUE);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 58:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",FALSE);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 59:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",TYPE_INT);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 60:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",TYPE_FLOAT);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
    case 61:
        if(ch == 32 || ch == '\n'){ //verifica se eh um caracter de espaço indicando a separação da palavra
            printf("%s\n",TYPE_CHAR);
            palavra_reservada_temp = "";
            inicio_palavra_reservada = 0;
            est = 0;
            return;
        }
        else{
            est = 45; // volta para verificações de palavras
            return;
        }
        return;


        if(ch == '\0'){
            printf("terminou \n");
            return;
        }

    }



     return;

}






#endif // ANALISE_LEXICA_H
