#include <stdio.h>
#include <string.h>

#include "include/tipos.h"

#include "include/analise_lexica.h"
#include "include/analise_sintatica.h"
#include "include/analise_semantica.h"


int n_simbolos = 0;

//void ler_arquivo();




int main(){

    char *buffer = NULL;

    buffer = ler_arquivo();
    n_simbolos = strlen(buffer);
    struct relatorio_lexico *relatorio_analise_lexixa[n_simbolos];
    struct relatorio_analise_sintatica *relatorio_analise_sintatica[n_simbolos];


//    for(int i=0;i<=sizeof(relatorio_analise_lexixa);i++){
//        printf("Estado: %d - ",relatorio_analise_lexixa[i]->estado);
//        printf("Simbolo: %s - ",relatorio_analise_lexixa[i]->token);
//        printf("Lexema: %c - \n",relatorio_analise_lexixa[i]->lexema);
//    }

    analisador_lexico(buffer,&relatorio_analise_lexixa);
    analisador_sintatico(buffer,relatorio_analise_sintatica);
    analisador_semantico(buffer);


    return 0;
}


