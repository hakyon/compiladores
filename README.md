### Universidade Tuiuti do Parana ###
### Ciência da Computação ###
### Disciplina Compiladores ###

### Exercicios/Trabalhos ###

* Periodo : 02/2018 - 07/2018
* Version 1.00
* Ambiente : Desktop

### Ferramentas usadas ###

* Terminal : C
* IDE : Visual Code
* Plataforma : Linux/Windows

### Assuntos ###

* Analise Léxixa
* Analise Semântica
* Analise Sintatica

### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com
